# votos


## Installation
~~~bash
git clone https://gitlab.com/lbmendes/votos.git
cd votos/backend
python -m venv venv
source venv/bin/activate
pip install -r requirements
~~~


## Usage

Inside directory `votos/backend`
~~~bash
uvicorn main:app
~~~

To check in the browser access http://127.0.0.1:8000/docs
