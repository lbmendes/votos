from fastapi import APIRouter
from schemas.votes import Vote


router = APIRouter()


@router.post("/votos", status_code=202)
def send_vote(vote: Vote):
    return vote
