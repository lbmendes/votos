from apis.version1 import route_votes
from fastapi import APIRouter


api_router = APIRouter()

api_router.include_router(route_votes.router, prefix="/votos", tags=["votos"])
