from apis.base import api_router
from fastapi import FastAPI


def start_application():
    app = FastAPI()
    app.include_router(api_router)
    return app


app = start_application()
