from enum import Enum
from pydantic import BaseModel


class Choice(str, Enum):
    VALUE_1 = 'cachorro'
    VALUE_2 = 'gato'
    VALUE_3 = 'papagaio'


class Vote(BaseModel):
    choice: Choice
